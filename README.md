# repl
Generic REPL trait

## Version
[![Crates.io](https://img.shields.io/crates/v/repl.svg)](https://crates.io/crates/repl)
[![Build Status](https://travis-ci.org/rustyhorde/repl.svg?branch=master)](travis)
